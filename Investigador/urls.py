from django.urls import path

from Investigador.views import RegistroInvestigador,index

urlpatterns = [
	path('',index,name='Index'),
    path('Registrar/', RegistroInvestigador.as_view(), name='Registrar'),
]