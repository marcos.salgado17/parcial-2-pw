from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import CreateView
from django.urls import reverse_lazy
from Investigador.forms import registroForm
# Create your views here.

class RegistroInvestigador(CreateView):
	model = User	
	template_name = 'Registrar.html'
	form_class = registroForm
	success_url = reverse_lazy('index.html')

def index(request):
	return render(request,'index.html')