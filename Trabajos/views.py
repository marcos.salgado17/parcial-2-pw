from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy


from .models import Trabajo 
# Create your views here.
def Investigaciones(request):
	return render(request,'Investigaciones.html')

class CrearTrabajo(generic.CreateView):
	template_name = "RegistrarInv.html"
	model = Trabajo
	files = [
		"Nombre",
		"Descripcion",
		"Categoria",
		"Author",
	]
	succes_url = reverse_lazy("Index")

def ListaTrabajo(request):
	queryset = Trabajo.objects.filter(Categoria="Ciencias Computacionales")
	context = {'Trabajo': queryset
	}
	return render(request,'ListaInv.html',context)

def ListaTrabajo1(request):
	queryset = Trabajo.objects.filter(Categoria="Ciencias de la Tierra")
	context = {'Trabajo': queryset
	}
	return render(request,'ListaInv.html',context)

def ListaTrabajo2(request):
	queryset = Trabajo.objects.filter(Categoria="Ciencias Naturales")
	context = {'Trabajo': queryset
	}
	return render(request,'ListaInv.html',context)

def ListaTrabajo3(request):
	queryset = Trabajo.objects.filter(Categoria="Ciencias Sociales")
	context = {'Trabajo': queryset
	}
	return render(request,'ListaInv.html',context)

def ListaTrabajo4(request):
	queryset = Trabajo.objects.filter(Categoria="Ciencias Medicas")
	context = {'Trabajo': queryset
	}
	return render(request,'ListaInv.html',context)