from django.db import models

from django.conf import settings

# Create your models here.
class Trabajo(models.Model):
	Nombre = models.CharField(max_length = 50)
	Descripcion = models.CharField(max_length = 250)
	Categoria = models.CharField(max_length = 50)
	Author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,)

