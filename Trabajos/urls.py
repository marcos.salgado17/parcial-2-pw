from django.urls import path

from Trabajos.views import CrearTrabajo,Investigaciones,ListaTrabajo,ListaTrabajo1,ListaTrabajo2,ListaTrabajo3,ListaTrabajo4

urlpatterns = [
      path('RegistrarInv/', CrearTrabajo.as_view(), name='RegistrarInv'),
	  path('Investigaciones/',Investigaciones,name='Investigaciones'),
	  path('ListaInv/',ListaTrabajo, name = 'ListaTrabajo'),
	  path('ListaInv/',ListaTrabajo1, name = 'ListaTrabajo1'),
	  path('ListaInv/',ListaTrabajo2, name = 'ListaTrabajo2'),
	  path('ListaInv/',ListaTrabajo3, name = 'ListaTrabajo3'),
	  path('ListaInv/',ListaTrabajo4, name = 'ListaTrabajo4'),

]